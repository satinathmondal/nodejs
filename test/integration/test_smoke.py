import subprocess
import time
from os import environ

import pytest
from grainger.dp.testutil.command import Command, cmd

FQDN = environ.get('FQDN')


class DNSNotReadyException(RuntimeError):
    pass


def define_dns_retry_function(timeout):
    def DNS_not_ready_error_filter(err, *args):
        if isinstance(err[1], DNSNotReadyException):
            print('retrying "{}()" in {} seconds'.format(args[0], timeout))
            time.sleep(timeout)
            return True
        return False

    return DNS_not_ready_error_filter


def define_retry_function(timeout):
    def waiting_error_filter(err, *args):
        if issubclass(err[0], subprocess.SubprocessError):
            return False
        print('retrying "{}()" in {} seconds'.format(args[0], timeout))
        time.sleep(timeout)
        return True

    return waiting_error_filter


@pytest.mark.flaky(max_runs=5, rerun_filter=define_dns_retry_function(10))
def test_1_app_dns_is_configured():
    completed_process = cmd(f'nslookup {FQDN}')
    if completed_process.returncode == 1:
        raise DNSNotReadyException()
    assert completed_process.returncode == 0


@pytest.mark.flaky(max_runs=5, rerun_filter=define_retry_function(15))
def test_2_app_is_up():
    cmd = f'curl --silent --show-error --insecure  https://{FQDN}/actuator/health/app'
    completed_process = Command(cmd)
    assert completed_process.body()['status'] == 'UP'


@pytest.mark.flaky(max_runs=5, rerun_filter=define_retry_function(15))
def test_3_db_is_up():
    cmd = f'curl --silent --show-error --insecure  https://{FQDN}/actuator/health/database'
    completed_process = Command(cmd)
    assert completed_process.body()['status'] == 'UP'
