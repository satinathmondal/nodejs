/**
 * TODO
 */
import { expect } from 'chai';
import Greeting from '../src/Greeting';

describe('Greeting', () => {

  describe('greeting.hello()', () => {

    it('should return welcome message for a guest user', () => {
      const greeting = new Greeting();
      const message = greeting.hello();
      expect(message).to.be.equal('Hello Guest');
    });

    it('should return welcome message for a named user', () => {
      const greeting = new Greeting('World');
      const message = greeting.hello();
      expect(message).to.be.equal('Hello World');
    });

  });

});