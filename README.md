
# Template Me Node - Starter Kit


Nodejs Starter Kit is  fully configured starter project for developing Node JS applications. You can use this kit to create your applications.



---

## Features

Following features are available as part of this starter template project

1. Hello World NodeJS application with index.js
2. Babel support for transpilation and backward compatibility
3. Docker build script to build the image for this application
4. Helm chart for k8s deployment 
5. Mocha & Chai (for test suites and assertions)
6. Eslint  config (for cleaner code)

---

## Repository & Other URLs

Repository : https://bitbucket.org/wwgrainger/template-me-node/src/master/


JIRA : https://jira.grainger.com/browse/CEMOD-293
Feature JIRA: https://jira.grainger.com/browse/CEMOD-510
